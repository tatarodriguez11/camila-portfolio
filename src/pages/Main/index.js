import React from 'react'
import Header from '../../components/Basic/Header'
import PersonalDescription from '../../components/Compound/PersonalDescription'
import GeneralContainer from '../../components/Basic/GeneralContainer'
import Separator from '../../components/Basic/Separator'
import RowOfProducts from '../../components/Compound/RowOfProducts'
import Footer from '../../components/Basic/Footer'
import {data} from '../../data'




class Main extends React.Component{
  render(){
    return(
      <>
      <Header title={data.headerTitle} logo={data.logo} subtitle={data.headerSubtitle}/>
      <GeneralContainer>
        
      <PersonalDescription 
        photo={data.profilePic}
        name={data.headerTitle}
        career={data.career}
        clients={data.clients}
        experienceYears={data.experienceYears}
        phone={data.phone}
        linkdirectcall={data.linkDirectCall}
        wspLink = {data.wspLink}
        phoneIcon={data.phoneIcon}
        wspImage={data.wspLogo}
        
        
        />
        <Separator />

        <RowOfProducts 
          image1 = {data.image1}
          image2 = {data.image2}
          image3 = {data.image3}
        />
        <RowOfProducts 
          image1 = {data.image1}
          image2 = {data.image2}
          image3 = {data.image3}
        />

        <RowOfProducts 
          image1 = {data.image1}
          image2 = {data.image2}
          image3 = {data.image3}
        />
   
    

      </GeneralContainer>

      <Footer logo={data.logo} title={data.headerTitle} phone={data.phone}/>
  
      </>
    )
  }
}



export default Main