import React from 'react'
import {connect} from 'react-redux'
import {addCounter, restCounter, logIn, logOut} from '../../store/actions'
import {auth} from '../../database'


class Counter extends React.Component{
  render(){
    return(
      <div>
        este es counter 
        {/* <p>{this.props.counter}</p>
        <button onClick={this.props.addCounter}>incrementar</button>
        <button onClick={this.props.restCounter}>restar</button> */}
       
        <button >login</button>
      </div>
    )
  }
}

const mapStateToProps = (state)=>{
  
  return{
    counter : state.counter,
    user: state.user
  }
}

const mapDispatchToProps = (dispatch, props)=>{
  return{
    addCounter : ()=>dispatch(addCounter()),
    restCounter : ()=>dispatch(restCounter()),
    userLogin : ()=>dispatch(logIn(props.user))
  }

}



export default connect(mapStateToProps , mapDispatchToProps)(Counter)