import React from 'react'
import Api from '../../api'


class Admin extends React.Component{

  onClick=()=>{
     Api.logOut()
  }

  render(){
    return(
      <div>
        admin panel
        <button
        onClick={
          this.onClick
        }
        >Logout</button>
      </div>
    )
  }
}

export default Admin