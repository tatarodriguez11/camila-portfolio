import React from 'react'
import Input from '../../components/Basic/Input'
import ActionButton from '../../components/Basic/ActionButton'
import GeneralContainer from '../../components/Basic/GeneralContainer'



class Login extends React.Component{

  state={
    email:"",
    password:"",
    logged:false

  }


  handleChange = (field) => (event) => {
    
    this.setState({
      [field]: event.target.value
    })
  }

  onClick=()=>{
    alert('estas iniciando sesión')
  }

  render(){
    return(
      <GeneralContainer>

      <form action="" style={{ display: 'flex', flexDirection: 'column' }}>
          <Input 
            titleInput="Email"
            type="email"
            placeholder="Email"
            required
            name="email"
            handleChange={this.handleChange("email")}
            
          />

          <Input 
            titleInput="Password"
            type="password"
            placeholder="Password"
            required
            min={6}
            name="password"
            handleChange={this.handleChange("password")}
            
            />

          <ActionButton onClick={this.onClick} >Iniciar Sesión</ActionButton>
      </form>
            </GeneralContainer>
    )
  }
}

export default Login