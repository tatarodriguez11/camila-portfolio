import profilePic from '../images/camila.JPG'
import iconLogo from '../images/flower.svg'
import wspLogo from '../images/whatsapp.png'
import phoneIcon from '../images/telephone.png'



export const data={
  phoneIcon,
  wspLogo,
  logo : iconLogo,
  profilePic,
  headerTitle: 'Camila Rodríguez',
  headerSubtitle:"Spa a domicilio",
  career : 'Cosmetóloga Esteticista',
  clients:" +1.200",
  experienceYears:"+2",
  phone: "+57 316 3771226",
  wspLink: "https://api.whatsapp.com/send?phone=573163771226&text=Hola,%20v%C3%AD%20tu%20p%C3%A1gina%20web,%20quisiera%20agendar%20una%20cita",
  linkDirectCall: "tel:+573163771226",
  image1: "https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  image2: "https://helpx.adobe.com/content/dam/help/en/stock/how-to/visual-reverse-image-search/jcr_content/main-pars/image/visual-reverse-image-search-v2_intro.jpg",
  image3: "https://images.pexels.com/photos/248797/pexels-photo-248797.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
}