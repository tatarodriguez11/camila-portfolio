import {auth} from '../database'


class Api {

  createUser (email, password){

   return auth.createUserWithEmailAndPassword(email, password)
    .catch((error)=>{
      const errorCode = error.code
      const errorMessage = error.message

      if (errorCode == 'auth/weak-password') {
        alert('The password is too weak.');
      } else {
        alert(errorMessage);
      }
      console.log(error);

    })

  }

  logOut(){
    return auth.signOut()
  }
}

export default new Api()