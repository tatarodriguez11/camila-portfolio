import React, {Fragment, useEffect} from 'react';
import {Route , Switch} from 'react-router-dom'
import Main from './pages/Main'
import Login from './pages/LogIn'
import CreateAccount from './pages/CreateAccount'
import Counter from './pages/Counter'
import Admin from './pages/Admin'
import store from './store'
import {Provider} from 'react-redux'
import {auth} from './database'
import {logIn , logOut} from './store/actions'


function App() {

  useEffect(()=>{
    auth.onAuthStateChanged((user)=>{
      if(user){
        alert('hay un usuario')
        
      }else{
        alert('usser logout')
      }
    })
  }, [])

  return (
    <Fragment>
      <Provider store={store}>
      <Switch>
        <Route exact path="/" component={Main} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/counter" component={Counter} />
        <Route exact path="/admin" component={Admin} />
        <Route exact path="/create-account" component={CreateAccount} />
      </Switch>
      </Provider>
    </Fragment>
  );
}

export default App;
