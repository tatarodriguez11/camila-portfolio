import * as Firebase from 'firebase';
import 'firebase/firestore'


const firebase = Firebase.initializeApp({
  apiKey: "AIzaSyCwjYbJJEGtwZsPC8VL5pYMB6FYDSdV6ew",
  authDomain: "camila-portfolio.firebaseapp.com",
  databaseURL: "https://camila-portfolio.firebaseio.com",
  projectId: "camila-portfolio",
  storageBucket: "camila-portfolio.appspot.com",
  messagingSenderId: "255855407404",
  appId: "1:255855407404:web:81fbd17e536b16bd"

});

export const db = firebase.firestore();
export const auth = firebase.auth();
export const storage = firebase.storage()
