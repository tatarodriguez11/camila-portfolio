
export function addCounter (){
  return{ type: 'ADD'}
}

export function restCounter (){
  return{ type: 'REST'}
}

export function logIn (user){
  return {
    type: 'LOGIN',
    payload : user
  }
}

export function logOut(){
  return{
    type: 'LOGOUT'
  }
}