import {createStore} from 'redux'

const state = {
  counter : 0,
  user:null
}

const reducer = (state, action) =>{
  switch (action.type) {
    case 'ADD':
      return{
        ...state,
        counter : state.counter + 1
      }
    case 'REST':
      return{
        ...state,
        counter : state.counter - 1
      }
    case 'LOGIN':
      return{
        ...state,
        user : action.payload
      }
    case 'LOGOUT':
      return{
        ...state,
        user : null
      }
  
    default:
      return state
    
  }
}

const store = createStore(reducer, state)

export default store