import React from 'react'
import './descriptionInformation.css'
import CallActionSection from '../../Basic/CallActionSection'


const DescriptionInformation = (props)=>{
  return(
    <div className="description-information-container">
      < CallActionSection 
      linkdirectcall={props.linkdirectcall} 
      phoneIcon={props.phoneIcon} 
      wspLink={props.wspLink}
      wspImage={props.wspImage}
      />
      <div className="description-information-statistics">
        < span className = "statistic" > < span className = "statistic-number" >{props.clients} </span> Clientes satisfechos  </span >
        < span className = "statistic" > < span className = "statistic-number" >{props.experienceYears} </span> Años de experiencia  </span >       
      
      </div>
      <div className="description-information-personal-data">
        <span className="personal-data-name">{props.name}</span>
        <span className="personal-data-career">{props.career}</span>
        <span className="personal-data-phone">{props.phone}</span>
      </div>
    </div>
  )
}

export default DescriptionInformation