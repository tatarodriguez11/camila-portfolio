import React, {useState} from 'react'
import './eachProductImage.css'
import {useSpring , animated} from 'react-spring'


const EachProductImage = ({image , marginRight})=>{

  const [open, toggle] = useState(false)
  const props = useSpring({
    opacity: open ? 1 : 0
  })


  return(
    <div className="each-product-image" 
    style={{backgroundImage:`url(${image})`, marginRight:`${marginRight}`}}
      onMouseEnter = { ()=>toggle(!open)}
      onMouseLeave={ ()=>toggle(!open)}
    >
      <animated.div className="each-product-hover" style={props}>

        {
          open && <span>Titulo del producto</span>
        }

      </animated.div>
    </div>
  )
}

export default EachProductImage