import React from 'react'
import {Link} from 'react-router-dom'
import './footer.css'


const Footer =(props)=>{
  return(
    <div className="footer-container">
      <div className="footer-border"></div>
      <div className="footer-info-container">

      <Link to="/" className="footer-link">
       <img className="footer-logo" src={props.logo} alt="flor de loto"/>
       <div style={{backgroundColor:"black", height:"28px", width:"1px", margin:"0 16px"}} />
       <h1 className="footer-title"> {props.title}</h1>    
      </Link>

      <div className="footer-contact-section">
        <span className="footer-contact">Contacto</span>
        <span className="footer-phone">{props.phone}</span>
      </div>

      <div className="footer-development-section">        
        < span > Ésta página fue hecha con <span role="img"> 💗 </span> por < a className="footer-link-tatiana" href = "https:/ / www.tatianaro.com / " > Tatiana Rodríguez </a> </span >
      </div>

      </div>

    </div>
  )
}

export default Footer