import React from 'react';
import './input.css'

function InputForm(props) {
  return (
    <div
      className="input-form-container"

    >
      <p className="input-form-title">{props.titleInput}</p>
      <input
        type={props.type}
        placeholder={props.placeholder}
        className="input-form"
        onChange={props.handleChange}
        name={props.name}
        required={props.required}
        min={props.min}
      >

      </input>
    </div>
  )
}

export default InputForm