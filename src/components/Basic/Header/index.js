import React from 'react'
import './header.css'
import {Link} from 'react-router-dom'
import {useSpring , animated} from 'react-spring'
import { useScrollYPosition } from 'react-use-scroll-position'


const Header =({logo , title, subtitle})=>{

  const scrollY = useScrollYPosition()
 
  
  const props = useSpring({
    // position : scrollY > 0 ? 'fixed' : "relative",
    height : scrollY > 0 ? "52px" : "77px"
  })
  const style = useSpring({
    fontSize : scrollY > 0 ? "20px" : "29px",
    color: scrollY > 0 ? "gray" : "black"
  })
  const barStyle = useSpring({
    backgroundColor: scrollY > 0 ? "gray" : "black"
  })

 

  return(
    <animated.header className="header" style={props} >
      <div className="content-wrapper">
      <Link to="/" className="header-link">
       <img className="header-logo" src={logo} alt="flor de loto"/>
       {/* <div className="header-first-bar" style={barStyle} /> */}
       <animated.h1 style={style}  className="header-title"> {title}</animated.h1>    
      </Link>
       <animated.div className="header-bar" style={barStyle}/>
        <p className="header-subtitle">{subtitle}</p>
      </div>
    </animated.header>
  )
}

export default Header