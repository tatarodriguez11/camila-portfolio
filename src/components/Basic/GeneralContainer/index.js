import React from 'react'
import './generalContainer.css'

const GeneralContainer = (props)=>{
  return(
    <div className="general-container">
      {props.children}
    </div>
  )
}

export default GeneralContainer

