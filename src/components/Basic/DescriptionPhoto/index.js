import React from 'react'
import './descriptionPhoto.css'



const DescriptionPhoto = (props)=>{
  return(
    <div className="photo-border">
      <div className="photo-white-space">
      <div className="photo" style={{backgroundImage:`url(${props.photo})`}}></div>
      </div>
    </div>
  )
}

export default DescriptionPhoto