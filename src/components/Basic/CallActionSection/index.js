import React from 'react'
import './callActionSection.css'
import ActionButton from '../../Basic/ActionButton'


const CallActionSection = (props)=>{
  return(
    <div className="call-action-section">
      <span className="call-action-contact">Contáctame</span>
      <div className="call-action-buttons-container">
        <ActionButton marginLeft="20px" link={props.linkdirectcall}> <img src={props.phoneIcon} alt="" style={{marginRight:"5px"}}/>  Llamar </ActionButton>
        <ActionButton link={props.wspLink} > <img src={props.wspImage} alt=""/> </ActionButton>
      </div>
    </div>
  )
}

export default CallActionSection
