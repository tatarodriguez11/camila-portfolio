import React from 'react'
import './actionButton.css'



const ActionButton = ({link, marginLeft, children, onClick})=>{

  return(
    <a href={link} target="_blank" className="action-button-link" rel="noopener noreferrer" >
    <button className="action-button" onClick={onClick} style={{marginLeft:`${marginLeft}`}}>
    { children}
    </button>
    </a>
  )
}

export default ActionButton