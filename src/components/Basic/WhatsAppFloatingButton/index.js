import React from 'react'
import './whatsappFloatingButton.css'

const WhatsAppFloatingButton = (props)=>{
  return(
    <a href={props.link} target="_blank">
      <div className="wsp-floating-btn">
        <img className="wsp-floating-btn-logo" src={props.wspLogo} alt=""/>
      </div>
    </a>
  )
}

export default WhatsAppFloatingButton