import React from 'react'
import DescriptionPhoto from '../../Basic/DescriptionPhoto'
import DescriptionInformation from '../../Basic/DescriptionInformation'
import './personalDescription.css'


const PersonalDescription =(props)=>{
  return(
    <div className="personal-description-container">
      <DescriptionPhoto photo={props.photo} />
      < DescriptionInformation 
      clients = {props.clients}
      experienceYears={props.experienceYears}
      name={props.name}
      career={props.career}
      phone={props.phone}    
      linkdirectcall={props.linkdirectcall}
      phoneIcon={props.phoneIcon}
      wspLink={props.wspLink}
      wspImage={props.wspImage}
      />
    </div>
  )
}

export default PersonalDescription