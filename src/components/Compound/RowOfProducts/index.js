import React from 'react'
import EachProductImage from '../../Basic/EachProductImage'
import './rowOfProducts.css'

const RowOfProduct = (props)=>{
  return(
    <div className="row-products">
      < EachProductImage 
        image = {props.image1} 
        marginRight="10px"
        />
        <EachProductImage 
        image = {props.image2}
        marginRight = "10px"

        />
        <EachProductImage
          image = {props.image3}
        />
    </div>

  )
}

export default RowOfProduct